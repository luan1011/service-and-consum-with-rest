<%-- 
    Document   : login
    Created on : Oct 2, 2019, 8:05:45 PM
    Author     : luan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
        <body>
        <div class="main">
            <form action="LoginServlet">
                <h1><span>ADMIN</span><label>Login</label></h1>
                <div class="inset">
                    <p>
                        <label for="email">USERNAME</label>
                        <input type="text" name="username" placeholder="" required />
                    </p>
                    <p>
                        <label for="password">PASSWORD</label>
                        <input type="password" name="password" placeholder="" required />
                    </p>
                    <p>
                        <input type="checkbox" name="remember" id="remember"/>
                        <label for="remember">Remember me</label>
                    </p>
                </div>
                <p class="p-container">
                    <span><a href="#">Forgot password ?</a></span>
                    <input type="submit" value="Login">
                </p>
            </form>
        </div>
        <!-- start copyright -->
        <div class="copy-right">
            <p>Design by <a href="#">FPT Aptech</a></p>
        </div>
        <!-- end copyright -->
    </body>
</html>
